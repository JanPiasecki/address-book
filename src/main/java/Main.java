import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Jan Piasecki on 29.04.2017.
 */

public class Main
{
    private static AddressBook addressBook = null;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args)
    {
        addressBook = new AddressBook("db.csv");
        System.out.println(addressBook);
        displayOptions();
    }

    private static void displayOptions()
    {
        boolean isInputInvalid;

        do
        {
            isInputInvalid = false;

            System.out.println("\nOPTIONS:\n[C]reate\n[R]ead\n[U]pdate\n[D]elete\n[E]xit");

            switch (scanner.nextLine().toLowerCase())
            {
                case "c":
                case "create":
                    displayOptionsCreate();
                    break;
                case "r":
                case "read":
                    displayOptionsRead();
                    break;
                case "u":
                case "update":
                    displayOptionsUpdate();
                    break;
                case "d":
                case "delete":
                    displayOptionsDelete();
                    break;
                case "e":
                case "exit":
                    displayOptionsExit();
                    break;
                default:
                    System.out.println("INVALID INPUT");
                    isInputInvalid = true;
                    break;
            }
        }
        while (isInputInvalid);
    }

    private static void displayOptionsCreate()
    {
        AddressBookRow abr = new AddressBookRow();

        System.out.println("OPTION >> CREATE:");
        abr.setId(addressBook.getMaxId() + 1);

        System.out.println("PROVIDE FIRST NAME");
        abr.setFirstName(scanner.nextLine());

        System.out.println("PROVIDE LAST NAME");
        abr.setLastName(scanner.nextLine());

        System.out.println("PROVIDE ADDRESS");
        abr.setAddress(scanner.nextLine());

        System.out.println("PROVIDE ZIP CODE");
        abr.setZip(scanner.nextLine());

        System.out.println("PROVIDE PHONE NUMBER");
        abr.setPhone(scanner.nextLine());

        System.out.println("PROVIDE EMAIL ADDRESS");
        abr.setEmail(scanner.nextLine());

        boolean isInputInvalid;

        do
        {
            isInputInvalid = false;

            System.out.println("SAVE RECORDS?");
            System.out.println(AddressBook.getTableHeader() + "\n" + abr + AddressBook.getTableFooter());
            System.out.println("[Y]es / [N]o");

            switch (scanner.nextLine().toLowerCase())
            {
                case "y":
                case "yes":
                    System.out.println("OPTION >> CREATE >> ADD >> YES");
                    addressBook.addRow(abr);
                    System.out.println(addressBook);
                    displayOptions();
                    break;
                case "n":
                case "no":
                    System.out.println("OPTION >> CREATE >> ADD >> NO");
                    System.out.println(addressBook);
                    displayOptions();
                    break;
                default:
                    System.out.println("INVALID INPUT");
                    isInputInvalid = true;
                    break;
            }
        }
        while (isInputInvalid);
    }

    private static void displayOptionsRead()
    {
        boolean isInputInvalid;

        do
        {
            isInputInvalid = false;

            System.out.println("OPTION >> READ >> SORT BY:\n[I]d\n[N]ame\n[Z]ip");
            String option = scanner.nextLine();

            switch (option.toLowerCase())
            {
                case "i":
                case "id":
                    System.out.println("OPTION >> READ >> SORT BY >> ID");
                    addressBook.sortById();
                    System.out.println(addressBook);
                    displayOptions();
                    break;
                case "n":
                case "name":
                    System.out.println("OPTION >> READ >> SORT BY >> NAME");
                    addressBook.sortByName();
                    System.out.println(addressBook);
                    addressBook.sortById();
                    displayOptions();
                    break;
                case "z":
                case "zip":
                    System.out.println("OPTION >> READ >> SORT BY >> ZIP");
                    addressBook.sortByZip();
                    System.out.println(addressBook);
                    addressBook.sortById();
                    displayOptions();
                    break;
                default:
                    System.out.println("INVALID INPUT");
                    isInputInvalid = true;
                    break;
            }
        }
        while (isInputInvalid);
    }

    private static void displayOptionsUpdate()
    {
        boolean isInputInvalid;

        do
        {
            isInputInvalid = false;

            System.out.println("OPTION >> UPDATE:");

            System.out.println("PROVIDE ID NUMBER");

            try
            {
                int id = Integer.parseInt(scanner.nextLine());

                AddressBookRow abr = addressBook.getAddressBookRowById(id);

                System.out.println(AddressBook.getTableHeader() + "\n" + abr + AddressBook.getTableFooter());

                AddressBookRow abrTemp = new AddressBookRow();

                abrTemp.setId(abr.getId());
                abrTemp.setFirstName(abr.getFirstName());
                abrTemp.setLastName(abr.getLastName());
                abrTemp.setAddress(abr.getAddress());
                abrTemp.setZip(abr.getZip());
                abrTemp.setPhone(abr.getPhone());
                abrTemp.setEmail(abr.getEmail());

                boolean isInput2Invalid;

                do
                {
                    isInput2Invalid = false;

                    System.out.println("OPTION >> UPDATE:\n[A]ddress\n[Z]ip\n[P]hone\n[E]mail");

                    switch (scanner.nextLine().toLowerCase())
                    {
                        case "a":
                        case "address":
                            System.out.println("PROVIDE NEW ADDRESS");
                            abrTemp.setAddress(scanner.nextLine());
                            break;
                        case "z":
                        case "zip":
                            System.out.println("PROVIDE NEW ZIP");
                            abrTemp.setZip(scanner.nextLine());
                            break;
                        case "p":
                        case "phone":
                            System.out.println("PROVIDE NEW PHONE");
                            abrTemp.setPhone(scanner.nextLine());
                            break;
                        case "e":
                        case "email":
                            System.out.println("PROVIDE NEW EMAIL");
                            abrTemp.setEmail(scanner.nextLine());
                            break;
                        default:
                            System.out.println("INVALID INPUT");
                            isInput2Invalid = true;
                            break;
                    }
                }
                while (isInput2Invalid);

                System.out.println("SAVE RECORDS?");

                System.out.println(AddressBook.getTableHeader() + "\n" + abrTemp + AddressBook.getTableFooter());

                System.out.println("[Y]es / [N]o");

                switch (scanner.nextLine().toLowerCase())
                {
                    case "y":
                    case "yes":
                        System.out.println("OPTION >> SAVE >> YES");
                        addressBook.setAddressBookRow(abrTemp);
                        System.out.println(addressBook);
                        displayOptions();
                        break;
                    case "n":
                    case "no":
                        System.out.println("OPTION >> SAVE >> NO");
                        System.out.println(addressBook);
                        displayOptions();
                        break;
                    default:
                        System.out.println("INVALID INPUT");
                        isInputInvalid = true;
                        break;
                }
            }
            catch (NumberFormatException ignored)
            {

                System.out.println("INVALID INPUT");
                isInputInvalid = true;

            }
            catch (NullPointerException ignored)
            {

                System.out.println("NO SUCH RECORD");
                isInputInvalid = true;
            }
        }
        while (isInputInvalid);

    }

    private static void displayOptionsDelete()
    {
        boolean isInputInvalid;

        do
        {
            isInputInvalid = false;

            System.out.println("OPTION >> DELETE:");

            System.out.println("PROVIDE ID NUMBER");

            try
            {
                int id = Integer.parseInt(scanner.nextLine());

                AddressBookRow abr = addressBook.getAddressBookRowById(id);

                System.out.println("DELETE RECORDS?");
                System.out.println(AddressBook.getTableHeader() + "\n" + abr + AddressBook.getTableFooter());
                System.out.println("[Y]es / [N]o");

                switch (scanner.nextLine().toLowerCase())
                {
                    case "y":
                    case "yes":
                        System.out.println("OPTION >> DELETE >> YES");
                        addressBook.deleteAddressBookRow(id);
                        System.out.println(addressBook);
                        displayOptions();
                        break;
                    case "n":
                    case "no":
                        System.out.println("OPTION >> DELETE >> NO");
                        System.out.println(addressBook);
                        displayOptions();
                        break;
                    default:
                        System.out.println("INVALID INPUT");
                        isInputInvalid = true;
                        break;
                }
            }
            catch (NumberFormatException ignored)
            {
                System.out.println("INVALID INPUT");
                isInputInvalid = true;
            }
            catch (NullPointerException ignored)
            {
                System.out.println("NO SUCH RECORD");
                isInputInvalid = true;
            }
        }
        while (isInputInvalid);
    }

    private static void displayOptionsExit()
    {
        boolean isInputInvalid;

        do
        {
            isInputInvalid = false;

            System.out.println("OPTION >> EXIT:");
            System.out.println("SAVE TO FILE?");
            System.out.println(addressBook);
            System.out.println("[Y]es / [N]o");

            switch (scanner.nextLine().toLowerCase())
            {
                case "y":
                case "yes":
                    System.out.println("OPTION >> EXIT >> SAVE TO FILE >> YES");
                    addressBook.saveToFile("db.csv");
                    System.exit(0);
                    break;
                case "n":
                case "no":
                    System.out.println("OPTION >> EXIT >> SAVE TO FILE >> NO");
                    System.exit(0);
                    break;
                default:
                    System.out.println("INVALID INPUT");
                    isInputInvalid = true;
                    break;
            }
        }
        while (isInputInvalid);
    }
}

class AddressBookRow
{
    private int id;
    private String firstName;
    private String lastName;
    private String address;
    private String zip;
    private String phone;
    private String email;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return String.format("| %4d | %12s | %12s | %24s | %8s | %12s | %24s |",
                id, firstName, lastName, address, zip, phone, email);
    }
}

class AddressBook
{
    private List<AddressBookRow> ab;
    private String dbName;

    public AddressBook(String dbName)
    {
        ab = new ArrayList<>();
        this.dbName = dbName;

        createDB(dbName);
        loadFromFile(dbName);
    }

    private static void createDB(String dbName)
    {
        Path path = Paths.get(dbName);
        if (Files.notExists(path))
        {
            try
            {
                Files.createFile(path);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void loadFromFile(String dbName)
    {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(dbName)))
        {
            String line;
            while ((line = br.readLine()) != null)
            {
                String[] lineSeparated = line.split(",");

                AddressBookRow row = new AddressBookRow();

                row.setId(Integer.parseInt(lineSeparated[0]));
                row.setFirstName(lineSeparated[1]);
                row.setLastName(lineSeparated[2]);
                row.setAddress(lineSeparated[3]);
                row.setZip(lineSeparated[4]);
                row.setPhone(lineSeparated[5]);
                row.setEmail(lineSeparated[6]);

                ab.add(row);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void saveToFile(String dbName)
    {
        StringBuilder csv = new StringBuilder();

        for (AddressBookRow abr : ab)
        {
            csv.append(
                    abr.getId() + "," +
                            abr.getFirstName() + "," +
                            abr.getLastName() + "," +
                            abr.getAddress() + "," +
                            abr.getZip() + "," +
                            abr.getPhone() + "," +
                            abr.getEmail() + "\n"
            );
        }

        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(dbName)))
        {
            bw.write(csv.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public AddressBookRow getAddressBookRowById(int id)
    {
        for (int i = 0; i < ab.size(); i++)
        {
            if (ab.get(i).getId() == id)
            {

                return ab.get(i);
            }
        }
        throw new NullPointerException();
    }

    public void setAddressBookRow(AddressBookRow abr)
    {
        for (int i = 0; i < ab.size(); i++)
        {
            if (ab.get(i).getId() == abr.getId())
            {
                ab.set(i, abr);
                return;
            }
        }
        throw new NullPointerException();
    }

    public void deleteAddressBookRow(int id)
    {
        for (int i = 0; i < ab.size(); i++)
        {
            if (ab.get(i).getId() == id)
            {
                ab.remove(i);
                return;
            }
        }
        throw new NullPointerException();
    }

    public void addRow(AddressBookRow abr)
    {
        ab.add(abr);
    }

    public int getMaxId()
    {
        int maxId = 0;

        for (AddressBookRow adr : ab)
        {
            if (adr.getId() > maxId) maxId = adr.getId();
        }

        return maxId;
    }

    public void sortById()
    {
        Collections.sort(ab, new Comparator<AddressBookRow>()
        {
            @Override
            public int compare(AddressBookRow row1, AddressBookRow row2)
            {
                return row1.getId() - row2.getId();
            }
        });
    }

    public void sortByName()
    {
        Collections.sort(ab, new Comparator<AddressBookRow>()
        {
            @Override
            public int compare(AddressBookRow row1, AddressBookRow row2)
            {
                return (row1.getLastName() + row1.getFirstName()).compareTo(row2.getLastName() + row2.getFirstName());
            }
        });

    }

    public void sortByZip()
    {
        Collections.sort(ab, new Comparator<AddressBookRow>()
        {
            @Override
            public int compare(AddressBookRow row1, AddressBookRow row2)
            {
                return row1.getZip().compareTo(row2.getZip());
            }
        });

    }

    @Override
    public String toString()
    {
        if (ab.isEmpty())
        {
            return "ADDRESS BOOK IS EMPTY";
        }

        String print = getTableHeader();

        for (AddressBookRow row : ab)
        {
            print = print + "\n" + row;
        }

        print += getTableFooter();

        return print;
    }

    public static String getTableHeader()
    {
        return String.format(
                "+------+--------------+--------------+--------------------------+" +
                        "----------+--------------+--------------------------+" +
                        "\n| %4s | %12s | %12s | %24s | %8s | %12s | %24s |" +
                        "\n+------+--------------+--------------+--------------------------+" +
                        "----------+--------------+--------------------------+",
                "ID", "FIRST NAME", "LAST NAME", "ADDRESS", "ZIP", "PHONE NO", "EMAIL");
    }

    public static String getTableFooter()
    {
        return "\n+------+--------------+--------------+--------------------------+" +
                "----------+--------------+--------------------------+";
    }
}
